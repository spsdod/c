#include <stdio.h>


/*
* Name: Add Numbers
* Using the assignment by sum operator +=
* Goal is to convert this to ARM assembly.
* Compile using GCC:
* gcc total.c -o total
*/

int main()
{
  int total;
  int i;

  total = 0;
  for (i=10; i>0; i--) {
    /*
     * Increase value of variable 'total' by the value of variable 'i', so it
     * will iterate through 'i' (starts at 10) and add 10,9,8,7,6,5,4,3,2,1 and
     * assign that value to 'total'
     */
    total += i;
  }
  printf("\n%d\n\n", total);
  return 0;
}
